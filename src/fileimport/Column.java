package fileimport;


public class Column {

    public Column(int index, String name) {
        this.index = index;
        this.name = name;
    }
    
    private String name = "";
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    private Integer index = -1;
    public Integer getIndex() {
        return index;
    }
    public void setIndex(Integer index) {
        this.index = index;
    }

}
