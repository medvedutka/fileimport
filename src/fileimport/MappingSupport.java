package fileimport;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Observable;


public class MappingSupport extends Observable {
    
    public static class MappingItem {
        private String key;
        public String getKey() {
            return key;
        }
        private String description;
        public String getDescription() {
            return description;
        }
        
        @Override
        public String toString() {
            if(description != null) {
                return description;
            }
            return key;
        }
    }
    
    public void addItem(String key) {
        addItem(key, null);
    }
    public void addItem(String key,String description) {
        MappingItem mi = new MappingItem();
        mi.key = key;
        mi.description = description;
        mappingItems.add(mi);
    }
    
    List<MappingItem> mappingItems = new LinkedList<MappingItem>();
    public List<MappingItem> getMappingItems() {
        return mappingItems;
    }
    
    Map<Column,MappingItem> map = new HashMap<Column,MappingItem>();
    
    public void map(Column column, MappingItem item) {
        if(item == null) {
            map.remove(column);
        }
        map.put(column, item);
        
        setChanged();
        notifyObservers();
    } 
    
    public MappingItem getMappingItem(Column column) {
        if(map.containsKey(column)) {
            return map.get(column);
        }
        return null;
    }
    
    public List<Column> getColumnsMappedToKey(String key) {
        List<Column> result = new LinkedList<Column>();
        for(Map.Entry<Column,MappingItem> entry : map.entrySet()) {
            if(key.equals(entry.getValue().key)) {
                result.add(entry.getKey());
            }
        }
        return result;
    }
    
    /**
     * @param key
     * @return -1 if no column is mapped to the key.
     */
    public int getFirstColumnsIdMappedToKey(String key) {
        List<Column> result = new LinkedList<Column>();
        for(Map.Entry<Column,MappingItem> entry : map.entrySet()) {
            if(key.equals(entry.getValue().key)) {
                return entry.getKey().getIndex();
            }
        }
        return -1;
    }
    
    public void clearMapping() {
        map.clear();
        setChanged();
        notifyObservers();
    }
    
    
}
