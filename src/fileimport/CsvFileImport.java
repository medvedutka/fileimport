package fileimport;

import au.com.bytecode.opencsv.CSVReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;

public class CsvFileImport extends FileImport {

    public static class CsvFileImportOptions extends FileImport.FileImportOptions {
        private String separator;
        public static final String PROP_SEPARATOR = "separator";
        public String getSeparator() {
            return separator;
        }
        public void setSeparator(String separator) {
            String oldSeparator = this.separator;
            this.separator = separator;
            propertyChangeSupport.firePropertyChange(PROP_SEPARATOR, oldSeparator, separator);
        }
        
        private Charset charset;
        public static final String PROP_CHARSET = "charset";
        public Charset getCharset() {
            return charset;
        }
        public void setCharset(Charset charset) {
            Charset oldCharset = this.charset;
            this.charset = charset;
            propertyChangeSupport.firePropertyChange(PROP_CHARSET, oldCharset, charset);
        }
    }

    
    
    
    private CsvFileImportOptions options;
    private CSVReader reader;
    private int currentRowIndex = -1;
    
    public void open(CsvFileImportOptions _options) throws Exception {
        options = _options;
        columns.clear();
        
        Reader fileReader = new InputStreamReader(new FileInputStream(options.file) , options.charset);
        if(options.separator.isEmpty()) {
            options.setSeparator(";");
        }
        reader = new CSVReader(fileReader,options.separator.charAt(0));
        
        if(options.firstLineContainColumnTitles) {
            String[] tokens = reader.readNext();currentRowIndex++;
            for(int i =0;i<tokens.length;i++) {
                if(columns.size() <= i) {
                    expandColumnDefinition();
                }
                columns.get(i).setName(tokens[i]);
            }
        }
    }
 
    @Override
    public void open(FileImportOptions _options) throws Exception {
        open((CsvFileImportOptions)_options);
    }    
    
    public CellValue[] readNext() {
        try {
            String[] tokens = reader.readNext();currentRowIndex++;
            if(tokens != null) {
                CellValue[] result = new CellValue[tokens.length];
                for(int i =0;i<tokens.length;i++) {
                    if(columns.size() <= i) {
                        expandColumnDefinition();
                    }
                    result[i] = new CellValue(columns.get(i),currentRowIndex,tokens[i].trim());
                }
                return result;
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    


}
