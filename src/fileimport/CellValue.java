package fileimport;


public class CellValue {
    
    public CellValue(Column column, int rowIndex, String value) {
        this.column = column;
        this.rowIndex = rowIndex;
        this.value = value;
    }
    
    private Column column;
    private int rowIndex;
    private String value;
    
    public Column getColumn() {
        return column;
    }
    public int getRowIndex() {
        return rowIndex;
    }
    public String getValue() {
        return value;
    }
    
}
