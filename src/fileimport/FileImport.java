package fileimport;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.ArrayList;


public abstract class FileImport {
 
    public static class FileImportOptions {
        File file;
        public static final String PROP_FILE = "file";
        public File getFile() {
            return file;
        }
        public void setFile(File file) {
            File oldFile = this.file;
            this.file = file;
            propertyChangeSupport.firePropertyChange(PROP_FILE, oldFile, file);
        }
    
        transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
        public void addPropertyChangeListener(PropertyChangeListener listener) {
            propertyChangeSupport.addPropertyChangeListener(listener);
        }
        public void removePropertyChangeListener(PropertyChangeListener listener) {
            propertyChangeSupport.removePropertyChangeListener(listener);
        }
        
        boolean firstLineContainColumnTitles;
        public static final String PROP_FIRSTLINECONTAINCOLUMNTITLES = "firstLineContainColumnTitles";
        public boolean isFirstLineContainColumnTitles() {
            return firstLineContainColumnTitles;
        }
        public void setFirstLineContainColumnTitles(boolean firstLineContainColumnTitles) {
            boolean oldFirstLineContainColumnTitles = this.firstLineContainColumnTitles;
            this.firstLineContainColumnTitles = firstLineContainColumnTitles;
            propertyChangeSupport.firePropertyChange(PROP_FIRSTLINECONTAINCOLUMNTITLES, oldFirstLineContainColumnTitles, firstLineContainColumnTitles);
        }
    }

    ArrayList<Column> columns = new ArrayList();
    public ArrayList<Column> getColumns() {
        return columns;
    }
    void expandColumnDefinition() {
        columns.add(new Column(columns.size(),""));
    }
    
    public abstract void open(FileImport.FileImportOptions _options) throws Exception;
    public abstract CellValue[] readNext();
}
