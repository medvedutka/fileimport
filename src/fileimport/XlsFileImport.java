package fileimport;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;



public class XlsFileImport extends FileImport {

    public static class XlsFileImportOptions extends FileImport.FileImportOptions {
        private int sheetNumber = 0;
        public static final String PROP_SHEETNUMBER = "sheetNumber";
        public int getSheetNumber() {
            return sheetNumber;
        }
        public void setSheetNumber(int sheetNumber) {
            int oldSheetNumber = this.sheetNumber;
            this.sheetNumber = sheetNumber;
            propertyChangeSupport.firePropertyChange(PROP_SHEETNUMBER, oldSheetNumber, sheetNumber);
        }
    }
    
    Iterator<Row> rowIterator;
    int currentRowIndex = 0;
    
    @Override
    public void open(FileImport.FileImportOptions options) throws Exception {
        open((XlsFileImport.XlsFileImportOptions)options);
    }
    
    public void open(XlsFileImport.XlsFileImportOptions options) throws Exception {
        columns.clear();
        FileInputStream file = new FileInputStream(options.getFile());
        HSSFWorkbook workbook = new HSSFWorkbook(file);
        HSSFSheet sheet = workbook.getSheetAt(options.getSheetNumber());
        rowIterator = sheet.iterator();
        
        if(options.firstLineContainColumnTitles) {
            if(!rowIterator.hasNext()) {
                throw new Exception("Le fichier est vide.");
            }
            
            Row row = rowIterator.next();currentRowIndex++;
            
            Iterator<Cell> cellIterator = row.cellIterator();
            int i = 0;
            while(cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                if(columns.size() <= i) {
                    expandColumnDefinition();
                }
                columns.get(i).setName(cellToString(cell));
                i++;
            }
        }
    }

    
    DataFormatter dataFormatter = new DataFormatter();
    private String cellToString(Cell cell) {
        return dataFormatter.formatCellValue(cell);
        /*cell.setCellType(Cell.CELL_TYPE_STRING);
        return cell.getStringCellValue();*/
    }
    
    
    @Override
    public CellValue[] readNext() {
        if(!rowIterator.hasNext()) {
            return null;
        }
        
        ArrayList<CellValue> result = new ArrayList<CellValue>();
        Row row = rowIterator.next();currentRowIndex++;
        int i = 0;
        Iterator<Cell> cellIterator = row.cellIterator();
        while(cellIterator.hasNext()) {
            Cell cell = cellIterator.next();
            if(columns.size() <= i) {
                expandColumnDefinition();
            }
            result.add(new CellValue(columns.get(i),currentRowIndex,cellToString(cell).trim()));
            i++;
        }
        return result.toArray(new CellValue[result.size()]);
    }
    
}
